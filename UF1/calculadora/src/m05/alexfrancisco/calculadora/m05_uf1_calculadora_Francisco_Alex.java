package m05.alexfrancisco.calculadora;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Label;
import swing2swt.layout.FlowLayout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class m05_uf1_calculadora_Francisco_Alex {

	protected Shell shell;
	private Text pantalla;
	private Button btn_8;
	private Button btn_9;
	private Button btn_4;
	private Button btn_5;
	private Button btn_6;
	private Button btn_1;
	private Button btn_2;
	private Button btn_3;
	private Button btn_0;
	private Button btn_resultat;
	private Button btn_divisio;
	private Button btn_multiplicacio;
	private Button btn_suma;
	private Button btn_resta;
	// Inicialització de variables
	double primerNumero;
	double segonNumero;
	String operador;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			m05_uf1_calculadora_Francisco_Alex window = new m05_uf1_calculadora_Francisco_Alex();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(277, 282);
		shell.setText("SWT Application");
		shell.setLayout(null);
		
		pantalla = new Text(shell, SWT.BORDER);
		pantalla.setBounds(23, 24, 218, 30);
		
		btn_0 = new Button(shell, SWT.NONE);
		btn_0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "0");
			}
		});
		btn_0.setText("0");
		btn_0.setBounds(23, 222, 40, 34);
		
		btn_1 = new Button(shell, SWT.NONE);
		btn_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "1");
			}
		});
		btn_1.setText("1");
		btn_1.setBounds(23, 174, 40, 34);
		
		btn_2 = new Button(shell, SWT.NONE);
		btn_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "2");
			}
		});
		btn_2.setText("2");
		btn_2.setBounds(76, 174, 40, 34);
		
		btn_3 = new Button(shell, SWT.NONE);
		btn_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "3");
			}
		});
		btn_3.setText("3");
		btn_3.setBounds(132, 174, 40, 34);
		
		btn_4 = new Button(shell, SWT.NONE);
		btn_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "4");
			}
		});
		btn_4.setText("4");
		btn_4.setBounds(23, 123, 40, 34);
		
		btn_5 = new Button(shell, SWT.NONE);
		btn_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "5");
			}
		});
		btn_5.setText("5");
		btn_5.setBounds(76, 123, 40, 34);
		
		btn_6 = new Button(shell, SWT.NONE);
		btn_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "6");
			}
		});
		btn_6.setText("6");
		btn_6.setBounds(132, 123, 40, 34);
		
		
		Button btn_7 = new Button(shell, SWT.NONE);
		btn_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "7");
			}
		});
		btn_7.setBounds(23, 73, 40, 34);
		btn_7.setText("7");
		
		btn_8 = new Button(shell, SWT.NONE);
		btn_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "8");
			}
		});
		btn_8.setText("8");
		btn_8.setBounds(76, 73, 40, 34);
		
		btn_9 = new Button(shell, SWT.NONE);
		btn_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Agafem el text actual de la pantalla i concatenem el número
				pantalla.setText(pantalla.getText() + "9");
			}
		});
		btn_9.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btn_9.setText("9");
		btn_9.setBounds(132, 73, 40, 34);
		
		btn_suma = new Button(shell, SWT.NONE);
		btn_suma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Guardem el número que hi ha en la pantalla
				primerNumero = Double.parseDouble(pantalla.getText());
				// Establim el operador
				operador = "+";
				// Posem la pantalla en blanc
				pantalla.setText("");
			}
		});
		btn_suma.setText("+");
		btn_suma.setBounds(201, 174, 40, 34);
		
		btn_resta = new Button(shell, SWT.NONE);
		btn_resta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Guardem el número que hi ha en la pantalla
				primerNumero = Double.parseDouble(pantalla.getText());
				// Establim el operador
				operador = "-";
				// Posem la pantalla en blanc
				pantalla.setText("");
			}
		});
		btn_resta.setText("-");
		btn_resta.setBounds(201, 222, 40, 34);
		
		btn_multiplicacio = new Button(shell, SWT.NONE);
		btn_multiplicacio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Guardem el número que hi ha en la pantalla
				primerNumero = Double.parseDouble(pantalla.getText());
				// Establim el operador
				operador = "*";
				// Posem la pantalla en blanc
				pantalla.setText("");
			}
		});
		btn_multiplicacio.setText("*");
		btn_multiplicacio.setBounds(201, 123, 40, 34);
		
		btn_divisio = new Button(shell, SWT.NONE);
		btn_divisio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Guardem el número que hi ha en la pantalla
				primerNumero = Double.parseDouble(pantalla.getText());
				// Establim el operador
				operador = "/";
				// Posem la pantalla en blanc
				pantalla.setText("");
			}
		});
		btn_divisio.setBounds(201, 73, 40, 34);
		btn_divisio.setText("/");
		
		btn_resultat = new Button(shell, SWT.NONE);
		btn_resultat.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// Inicialitzem el resultat
				double resultat = 0;
				String resultatStr;
				// Guardem el segon número
				segonNumero = Double.parseDouble(pantalla.getText());
				// Comprobem l'operador i depenent de quin es, mostrem un resultat
				switch(operador) {
					case "+":
						// Fem la suma
						resultat = primerNumero + segonNumero;
						// El convertim a String
						resultatStr = Double.toString(resultat);
						// El mostrem
						pantalla.setText(resultatStr);
						break;
					case "-":
						// Fem la resta
						resultat = primerNumero - segonNumero;
						// El convertim a String
						resultatStr = Double.toString(resultat);
						// El mostrem
						pantalla.setText(resultatStr);
						break;
					case "*":
						// Fem la multiplicació
						resultat = primerNumero * segonNumero;
						// El convertim a String
						resultatStr = Double.toString(resultat);
						// El mostrem
						pantalla.setText(resultatStr);
						break;
					case "/":
						// Fem la divisio
						resultat = primerNumero / segonNumero;
						// El convertim a String
						resultatStr = Double.toString(resultat);
						// El mostrem
						pantalla.setText(resultatStr);
						break;
						
				}
			}
		});
		btn_resultat.setBounds(76, 222, 94, 34);
		btn_resultat.setText("=");

	}
}
