package people;

public class Main {

	public static void main(String[] args) {
		// Creation of the Person's object
		Person p = new Person("Alex", 22, "alex@alex.es", "613872811");
		// Print person's info
		System.out.println(p.toString());
	}

}
