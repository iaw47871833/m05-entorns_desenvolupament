# Conceptes bàsics de programació

Tenim dos tipus de llenguatges de programació:

- **Compilats**: Son aquells llenguatges que requereixen un pas adicional abans de ser executats, la **compilació**, que converteix el codi que escribim a llenguatge màquina.
Per exemple: C, Java

- Interpretat: converteix el llenguatge codi font a codi màquina a mesura que es va executant. Per exemple: Javascript, Pascal, PHP.

### Funcionament de Java

Per una banda tenim el codi font (necessari per poder compilar el programa) del programador, per exemple l'arxiu ```Myprogram.java```. Llavors amb un compilador el que fem es convertir el codi font a bytecodes, que és el llenguatge que entén la màquina virtual Java.

Finalment la màquina virtual interpreta els bytecodes i els tradueix a binari.

![Image](./img/compilationJava.gif)

Per executar un programa fem:

```
javac MyProgram.java
```

Si es queda penjat ens afrontarem a un bug o un error d'escabilitat.

Amb aquesta ordre <ins>compilem</ins> (creació d'un fitxer amb la traducció del codi font a codi màquina) i obtenim el fitxer ```MyProgram.class```.

A continuació utilitzem:

```
java MyProgram
```

On <ins>interpretem</ins> el fitxer MyProgram.class.

En el cas de treballar amb un llenguatge de programació interpretat i el volem executar només necessitarem el codi font del programa.

### JDK, JRE, JVM

* **JDK - Java Development Kit**: és l'entorn per desenvolupar i executar el programa Java. 
Inclou:
    * Eines de desenvolupament
    * JRE (per executar el programa java)

El JDK conté una JVM privada i altres recursos com un interpretador (**java**), un compilador (**javac**), un archivador (**jar**), un generador de documentació (**Javadoc**)

Amb el JDK obtenim les ordres ```java``` i ```javac```.


* **JRE - Java Runtime Environment**: és en entorn que serveix per crear aplicacions amb Java i executar el programa java a una màquina. Es una implementació del JVM. Existeix físicament i conté llibreries i altres arxius que el JVM utilitza.

Amb el JRE disposem de la comanda ```java```

* **JVM - Java Virtual Machine (JVM)**. S'anomena maquina virtual, perquè físicament no existeix. Aquesta proporciona un entorn on el bytecode (El codi que s'obté en el procés de compilació d'un programa Java) s'executa línia a línia (interpretació). També pot executar programes escrits en altres llenguatges i compilats a bytecode de Java.

![Image](./img/jdk_jre_jvm.png)