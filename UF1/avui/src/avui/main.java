package avui;

import java.util.Date;
import java.text.SimpleDateFormat;

public class main {

	public static void main(String[] args) {
		// Create a DateTime object to get the current date
		Date currentDate = new Date();
		// Format date
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strFormat = formatter.format(currentDate);
		// Transform the DateTime object to String
		System.out.println("Current date: " + strFormat);
	}

}
