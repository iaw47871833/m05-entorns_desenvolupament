/*
 * Boat.java
 * 		
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

package harbour;

/**
 * Simulates a boat.
 */
public abstract class Boat {

    /** Boat registration plate */
    private String regPlate;
    /** Boat length */
    private double length;
    /** Boat fabrication year */
    private int year;

    // Constructor

	/**
	 * @param regPlate
	 * @param length
	 * @param year
	 */
	public Boat(String regPlate, double length, int year) {
		super();
		this.regPlate = regPlate;
		this.length = length;
		this.year = year;
	}

	/**
     * Calculates the amount according to the boat.
     * 
     * @return the amount
     */
    public abstract double amountAccordingToBoat();

    /**
     * Calculates the base amount for all boats.
     * 
     * @return the amount
     */
    public double baseAmount() {
        return this.length * 10;
    }

	/**
	 * @return the regPlate
	 */
	public String getRegPlate() {
		return regPlate;
	}

	/**
	 * @param regPlate the regPlate to set
	 */
	public void setRegPlate(String regPlate) {
		this.regPlate = regPlate;
	}

	/**
	 * @return the length
	 */
	public double getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(double length) {
		this.length = length;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((regPlate == null) ? 0 : regPlate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boat other = (Boat) obj;
		if (regPlate == null) {
			if (other.regPlate != null)
				return false;
		} else if (!regPlate.equals(other.regPlate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Boat [regPlate=" + regPlate + ", length=" + length + ", year=" + year + "]";
	}
	
	

    // Getters & setters
    
	// TODO

    // equals & hashCode
    
    // TODO

}
