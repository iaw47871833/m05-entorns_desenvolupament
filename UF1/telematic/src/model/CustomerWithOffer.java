package model;

public class CustomerWithOffer extends Customer {
	/** Customer's name */
	private String name;
	/** Customer's dni */
	private String dni;
	/** The price the Customer must pay for each minute of connectivity */
	protected double priceMinute;

	

	public CustomerWithOffer(String name, String dni, double priceMinute) {
		super(name, dni, priceMinute);
		// TODO Auto-generated constructor stub
	}



	/**
	 * Calculates the bill amount. Customers with offer 1 pay the total of minutes
	 * by the price per minute minus a discount, but the 3 first minutes are free.
	 * 
	 * @return the bill amount
	 */
	public double billAmount() {
		// TODO Auto-generated method stub
		return 0;
	}
}
