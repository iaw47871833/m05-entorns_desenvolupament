/*
 * Company.java        1.1 24/09/2013
 *   
 * Copyright 2011-2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package model;

import java.util.HashSet;

/**
 * Describes a telematic company.
 * 
 * @author Mònica Ramírez Arceda
 * @version 1.1 24/09/2013
 */
public class Company {

    /** Customers of the company */
    private HashSet<Customer> Customers;

    /**
     * Company constructor.
     */
    public Company() {
 this.Customers = new HashSet<Customer>();
    }

    /**
     * Add a Customer to the company.
     * 
     * @param u a Customer
     * @return true if the Customer has been added, false otherwise
     */
    public boolean add(Customer u) {
  return this.Customers.add(u);
    }

    /**
     * List all Customers of the company. For each Customer the report details Customer's dni, name, number of
     * connections, total of minutes, amount to be paid by the Customer, and what kind of offer he/she
     * has.
     */
    public void listCustomers() {
        System.out.println("CLIENTS A FACTURAR");
        System.out.println("==================");
        System.out.printf("%-10s%-20s%-3s%8s%10s%7s\n",
                          "DNI",
                          "NOM",
                          "CON",
                          "MINS",
                          "IMPORT",
                          "OFERTA");
        for (Customer u : Customers) {
System.out.printf("%-10s%-20s%3d%8.2f%10.2f", u.getDni(), u.getName(), u
        .getConnections().size(), u.totalMinutes(), u.billAmount());
if (u instanceof CustomerOffer1) {
    System.out.printf("%5s", "1");
} else if (u instanceof CustomerOffer2) {
    System.out.printf("%5s", "2");
} else if (u instanceof CustomerOffer3) {
    System.out.printf("%5s", "3");
}
System.out.println("");
        }
    }
}
