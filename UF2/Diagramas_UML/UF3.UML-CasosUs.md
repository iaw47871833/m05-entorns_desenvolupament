### Exercici 1

Se desea desarrollar un sistema de encuentros virtuales (parecido a un chat).

* Cuando se conecta al servidor, un usuario puede entrar o salir de un encuentro.
* Cada encuentro tiene un manager.
* El manager es el usuario que ha planificado el encuentro (el nombre del encuentro, la agenda del encuentro y el moderador del encuentro).
* Cada encuentro puede tener también un moderador designado por el manager.
* La misión del moderador es asignar los turnos de palabra para que los usuarios hablen.
* El moderador también podrá dar por concluido el encuentro en cualquier momento.
* En cualquier momento un usuario puede consultar el estado del sistema, por ejemplo los encuentros planeados y su información

![Exercici1](./img/Exercici1CasosUs.png)

### Exercici 2

Un juego de teléfono móvil dónde participan dos jugadores cada uno con su propia terminal.

* Cuando dos jugadores desean jugar, uno de ellos crea una nueva partida y el otro se conecta.
* El objetivo del juego es manejar una nave y disparar al contrario. Si uno de los dos jugadores acierta, la partida termina.
* Si uno de los dos jugadores deja la partida (o se pierde la conexión) la partida termina.

![Exercici2](./img/Exercici2CasosUs.png)

### Exercici 3

Un sistema personal de bolsa se conecta periódicamente a servidores que ofrecen información de las cotizaciones.

* El sistema personal permite marcar una serie de valores para realizar un seguimiento  y consultar los datos de dichos valores.
* Si a la hora de actualizar las cotizaciones uno de los valores marcados presenta una gran subida o bajada, informará a usuario de ello.

![Exercici3](./img/Exercici3CasosUs.png)

### Exercici 4

Un sistema automático de cambio de grupos para asignaturas funciona de la siguiente manera:

* El profesor da de alta una asignatura y proporciona al sistema un listado con los alumnos matriculados en dicha asignatura.
* Un alumno que quiera cambiar de grupo en una asignatura puede consultar las peticiones de cambio.
* Si encuentra alguna que le interese, el alumno solicita el cambio y el sistema lo almacena.
* Si no, el alumno puede dejar el cambio que desea por si a otro alumno le interesara.
* Los alumnos sólo pueden consultar y publicitar cambios de las asignaturas en las que están matriculados.

![Exercici4](./img/Exercici4CasosUs.png)

### Exercici 5

Se necesita realizar un sistema que permita el acceso al personal del departamento de compras el hacer pedidos.

* Se quiere contemplar el que haya pedidos urgentes.
* Una vez hecho el pedido se quiere poder realizar un seguimiento del mismo
* Tanto para hacer el seguimiento como para poder realizar pedidos hay que estar validado en el sistema, que se puede hacer conociendo la clave o por reconocimiento de la retina.

![Exercici5](./img/Exercici5CasosUs.png)