# Documentació YouTrack

El projecte té diferents apartats que és amb el que ens hem guiat per fer cada tasca.

Dividirem les tasques en diferent moduls o apartats, on crearem swimlanes per cada modul (usuari, empresa, oferta...) de la pàgina web, on dins de cada una vindran les diferents funcionalitats que tindrà.

Les cartes estaran dividides en "front-end" i "back-end". 

A la part de "front-end" aniran totes les etiquetes relacionades amb les assignatures de m09-disseny, relacionades amb les tasques de maquetacions i els estils i m06-javascript on aniran els mòduls i serveis.

En la part de "back-end" aniran les etiquetes relacionades amb l'assignatura de m07-php on farem totes les tasques relacionades amb formularis, bases de dades, vistes, etc...

En el cas de que siguin tasques de menys importancia com per exemple, fer la documentació, escollir plantillas, aniran en una altre etiqueta que anomenarem "Extras".

Cada tasca vindra donada amb l'etiqueta de l'assignatura i el llenguatge de programació (Javascript, PHP, HTML...) o framework (Laravel, Angular, React) a utilitzar.

En el cas de que siguin tasques molt grans que requereixin de més temps, dividirem la tasca en petites porcions on cada carta tindrà el mateix nom acompanyat d'una petita descripció del pas a seguir per aconseguir l'objectiu.

Per exemple crear la base de dades, vindra amb les tasques: pensar en el cos de la BD, crear la BD, insertar registres, etc.

Per cada tasca nova indicarem la prioritat segons si són urgents o no i aniran assignada a un membre de l'equip amb una data limit segons el temps que creem que es apropiat per acabarla. Quan un dels membres comenci a fer la tasca, es mouran cap a la columna de "En procés" i al acabar-les, es mouran a la columna de "Verificació", on necessitarem l'ajuda del professorat per confirmar que podem donar la tasca per acabada i moura-l'ha a la columna de les tasques "acabades".

Al estar utilitzant el el métode agil "Scrum", s'anirant crean sprints amb el seu objectiu i la data a completar, on al final de cada sprint parlarem entre nosaltres per veure com veiem el treball, que es el esta bé i que malament.

Si hi ha alguna tasca que conté algun tipus de error, es ficarà una nova tasca amb una etiqueta "Bug" indicant l'error, per tal de que tots els membres en cas de trobar la solució, eliminar la tasca. 