package veterinary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author Alex Francisco Tinaya
 * iaw47871833
 */
class VisitWithoutDiscountTest {
	
	Visit v1;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		v1 = new VisitWithoutDiscount("23/05/2021", 'M');
	}

	@Test
	void testCalculateCost() {
		v1.setGrossPrice(30);
		assertEquals(v1.calculateCost(), 36.3, 0);
	}

}
