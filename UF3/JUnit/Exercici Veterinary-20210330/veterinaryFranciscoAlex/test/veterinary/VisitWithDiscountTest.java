package veterinary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/**
 * @author Alex Francisco Tinaya
 * iaw47871833
 */
class VisitWithDiscountTest {
	Visit v1;
	
	@BeforeEach
	void setUp() throws Exception {
		v1 = new VisitWithDiscount("20/07/2021", 'M');
	}

	@Test
	void testCalculateCostJuly() {
		v1.setGrossPrice(30);
		assertEquals(v1.calculateCost(), 30.855, 0);
	}
	
	@Test
	void testCalculateCostAugust() {
		Visit v2 = new VisitWithDiscount("20/08/2021", 'D');
		v2.setGrossPrice(40);
		assertEquals(v2.calculateCost(), 41.14, 0);
	}
	
	@Test
	void testCalculateCostNotSummer() {
		Visit v3 = new VisitWithDiscount("15/03/2021", 'A');
		v3.setGrossPrice(35);
		assertEquals(v3.calculateCost(), 38.115, 0);
	}

}
