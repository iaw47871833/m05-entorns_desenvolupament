package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import isLeap.YearUtilities;

class LeapTest {
	
	private YearUtilities yu;

	@BeforeEach
	void setUp() throws Exception {
		yu = new YearUtilities();
	}
	
	@Test
	public void testMultiple400() {
		assertEquals(true, yu.isLeap(1600));
	}

	@Test
	public void testMultiple4No100() {
		assertEquals(true, yu.isLeap(2012));
	}
	
	@Test
	public void testMultiple4Multiple4Multiple100() {
		assertEquals(false, yu.isLeap(1700));
	}
	
	@ParameterizedTest
	@ValueSource(ints= {1600, 2012, 1700, 2016, 2015})
	public void paramTestLeapYears(int year) {
		assertEquals(true, yu.isLeap2(year));
	}
	
}
