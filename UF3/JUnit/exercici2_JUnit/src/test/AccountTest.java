package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import account.Account;
import isLeap.YearUtilities;

class AccountTest {

	private Account ac1;
	
	@BeforeEach
	void setUp() throws Exception {
		ac1 = new Account("Alex", 123456789, 1000);
	}

	/**
	 * Test per fer un ingres amb exit
	 */
	@Test
	public void testIngresar() {
		assertEquals(true, ac1.ingresar(1000));
	}
	
	/**
	 * Test per fer un ingres a partir d'una quantitat negativa
	 */
	@Test
	public void testNoIngressar() {
		assertEquals(false, ac1.ingresar(-500));
	}
	
	/**
	 * Test per treure diners amb amount negatiu
	 */
	@Test
	public void testTreureDinersAmountNegatiu() {
		float amount = -500;
		float comisio = 1000;
		assertEquals(false, ac1.treureDiners(amount, comisio));
	}
	
	/**
	 * Test per treure diners amb comissio negatiu
	 */
	@Test
	public void testTreureDinersComisioNegatiu() {
		float amount = 1500;
		float comisio = -1000;
		assertEquals(false, ac1.treureDiners(amount, comisio));
	}
	
	/**
	 * Test per treure diners amb amount i comisionegatiu
	 */
	@Test
	public void testTreureDinersAmountComisioNegatiu() {
		float amount = -500;
		float comisio = -1000;
		assertEquals(false, ac1.treureDiners(amount, comisio));
	}
	
	/**
	 * Test per treure diners a partir de la suma d'amount i comisio superior al balance
	 */
	@Test
	public void testTreureDinersSuperiorBalance() {
		float amount = 1500;
		float comisio = 1000;
		assertEquals(false, ac1.treureDiners(amount, comisio));
	}
	
	/**
	 * Test per treure diners a partir de la suma d'amount i comisio inferior al balance
	 */
	@Test
	public void testTreureDiners() {
		Account ac2 = new Account("Alex", 12345789, 5000);
		float amount = 1500;
		float comisio = 1000;
		assertEquals(true, ac2.treureDiners(amount, comisio));
	}
	
	
	/**
	 * Test per transferir a un compte amb balance insuficient
	 */
	@Test
	public void testTranferBalanceInsuficient() {
		Account ac2 = new Account("Alex", 12345789, 3000);
		float quantitat = 2000;
		assertEquals(false, ac2.transfer(ac2, quantitat));
	}
	
	/**
	 * Test per transferir a un compte amb balance suficient
	 */
	@Test
	public void testTranfer() {
		Account ac2 = new Account("Alex", 12345789, 3000);
		float quantitat = 700;
		assertEquals(true, ac2.transfer(ac2, quantitat));
	}
	

}
