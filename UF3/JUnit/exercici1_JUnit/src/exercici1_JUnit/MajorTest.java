package exercici1_JUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class MajorTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		Major major = new Major();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testMayor() {
		int numeros[] = {3, 7, 9, 8};
		assertEquals(9, Major.mayor(numeros));
	}
	
	@Test
	@Disabled
	void testPosicionsDiferents() {
		// Comprobem amb llistes amb posicions diferents de l'element major
		assertEquals(9, Major.mayor(new int[] {9, 7, 8}));
		assertEquals(9, Major.mayor(new int[] {7, 9, 8}));
		assertEquals(9, Major.mayor(new int[] {7, 8, 9}));
	}
	
	@Test
	void testDuplicats() {
		// Comprobem una llista amb duplicats
		assertEquals(9, Major.mayor(new int[] {9, 7, 9, 8}));
	}
	
	@Test
	@Disabled
	void testUnicValor() {
		// Cas d'un únic valor
		assertEquals(7, Major.mayor(new int[] {7}));
	}

	@Test
	void testTotsNegatius() {
		// Cas que tots siguin negatius
		assertEquals(-4, Major.mayor(new int[] {-4, -6, -7}));
	}
	
	@Test
	void testCapValor() {
		// Cas que no contingui cap valor
		assertThrows(RuntimeException.class, () -> {
			Major.mayor(new int[] {});
		});
	}
}
