package travelagency;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HikingTest {

    private Trip t1;
    private Person a1;
    private Group g1;

    @BeforeEach
	void setUp() throws Exception {
        t1 = new Hiking("Carros de foc", 2, "10/06/2021", "10/06/2021", 30);
        a1 = new Adult("11", "Joan Grau");
        g1 = new Group(); 
	}
    
    @Test
    public void testMaxPeople() {
    	Adult a2 = new Adult("12345678B", "Alex Francisco");
    	Adult a3 = new Adult("12345678C", "Ramon Carrer");
    	Adult a4 = new Adult("12345678D", "Maria Dolors");
    	// Afegim persones al grup
    	g1.add(a2);
    	g1.add(a3);
    	g1.add(a4);
    	// Afegim el grup al trip
    	t1.add(g1);
    	Assert.assertEquals(false, t1.groupFits(g1));
    }
    
    @Test
    public void testFitsPeople() {
    	Adult a2 = new Adult("12345678B", "Alex Francisco");
    	Adult a3 = new Adult("12345678C", "Ramon Carrer");
    	// Afegim persones al grup
    	g1.add(a2);
    	g1.add(a3);
    	// Afegim el grup al trip
    	t1.add(g1);
    	Assert.assertEquals(true, t1.groupFits(g1));
    }
    
    @Test
    public void testAddChild() {
    	// Creem un adult
    	Adult a2 = new Adult("12345678B", "Alex Francisco");
    	// Creem un nen
    	Child c1 = new Child("1234568W", "Francesc Ferreon");
    	// Els afegim al grup
    	g1.add(a2);
    	g1.add(c1);
    	Assert.assertEquals(false, t1.add(g1));
    }

    @Test
    public void testPriceMes6() {
        g1.add(a1);
        t1.add(g1);
        Assert.assertEquals(30., t1.pricePerPerson(), 0);
    }
    
    @Test
    public void testPriceMes1() {
    	Trip t2 = new Hiking("Cavalls del vent", 5, "10/01/2021", "10/06/2021", 30);
    	Person a2 = new Adult("11", "Joan Grau");
        Group g2 = new Group();
        g2.add(a2);
        t2.add(g2);
        Assert.assertEquals(33., t2.pricePerPerson(), 0);
    }
    
    @Test
    public void testPriceMes3() {
    	Trip t2 = new Hiking("Cavalls del vent", 5, "10/03/2021", "10/03/2021", 30);
    	Person a2 = new Adult("11", "Joan Grau");
        Group g2 = new Group();
        g2.add(a2);
        t2.add(g2);
        Assert.assertEquals(33., t2.pricePerPerson(), 0);
    }
    
    @Test
    public void testPriceMes2() {
    	Trip t2 = new Hiking("Cavalls del vent", 5, "21/02/2021", "21/02/2021", 30);
    	Person a2 = new Adult("11", "Joan Grau");
        Group g2 = new Group();
        g2.add(a2);
        t2.add(g2);
        Assert.assertEquals(33., t2.pricePerPerson(), 0);
    }
    
    @Test
    public void testPriceMes12() {
    	Trip t2 = new Hiking("Cavalls del vent", 5, "03/12/2021", "03/12/2021", 30);
    	Person a2 = new Adult("11", "Joan Grau");
        Group g2 = new Group();
        g2.add(a2);
        t2.add(g2);
        Assert.assertEquals(33., t2.pricePerPerson(), 0);
    }
    

}
