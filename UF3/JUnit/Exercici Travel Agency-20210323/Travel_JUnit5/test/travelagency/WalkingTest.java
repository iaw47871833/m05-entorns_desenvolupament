package travelagency;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WalkingTest {
	
	private Trip t1;
    private Person a1;
    private Group g1;

	@BeforeEach
	void setUp() throws Exception {
        t1 = new Walking("Paseo por la montaña", 2, "10/06/2021", "10/06/2021", 30);
        a1 = new Adult("11", "Joan Grau");
        g1 = new Group(); 
	}
	
	@Test
	public void testMaxPeople() {
		// Creem persones
		Adult a2 = new Adult("12345678B", "Alex Francisco");
    	Adult a3 = new Adult("12345678C", "Ramon Carrer");
    	Adult a4 = new Adult("12345678D", "Maria Dolors");
    	// Els afegim al group
    	g1.add(a2);
    	g1.add(a3);
    	g1.add(a4);
    	// Afegim el grup al trip
    	t1.add(g1);
    	Assert.assertEquals(false, t1.groupFits(g1));
	}
	
	@Test
	public void testFitsPeople() {
		Trip t2 = new Walking("Paseo por la montaña", 2, "10/06/2021", "10/06/2021", 30);
		Group g2 = new Group(); 
		// Creem persones
		Adult a2 = new Adult("12345678B", "Alex Francisco");
    	Adult a3 = new Adult("12345678C", "Ramon Carrer");
    	// Els afegim al group
    	g2.add(a2);
    	g2.add(a3);
    	// Afegim el grup al trip
    	t2.add(g1);
    	Assert.assertEquals(true, t2.groupFits(g2));
	}
	
	@Test
	public void addChild() {
		Child c1 = new Child("123456K", "Martina Romeo");
		g1.add(c1);
		assertEquals(false, t1.add(g1));
	}
	
	@Test
	public void testPricePeople() {
		Trip t3 = new Walking("Paseo por la montaña", 2, "10/06/2021", "10/06/2021", 30);
		Group g3 = new Group(); 
		// Creem persones
		Adult a2 = new Adult("12345678B", "Alex Francisco");
    	Adult a3 = new Adult("12345678C", "Ramon Carrer");
    	// Els afegim al group
    	g3.add(a2);
    	g3.add(a3);
    	// Afegim el grup
    	t3.add(g3);
		Assert.assertEquals(27, t3.pricePerPerson(), 0);
	}
	
	@Test
	public void testPricePeople2() {
		Trip t4 = new Walking("Paseo por la montaña", 5, "10/06/2021", "10/06/2021", 30);
		Group g4 = new Group(); 
		// Creem persones
		Adult a2 = new Adult("12345678B", "Alex Francisco");
    	Adult a3 = new Adult("12345678C", "Ramon Carrer");
    	// Els afegim al group
    	g4.add(a2);
    	g4.add(a3);
    	// Afegim el grup
    	t4.add(g4);
		Assert.assertEquals(30, t4.pricePerPerson(), 0);
	}

}
