package travelagency;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TravelAgencyTest {

    private TravelAgency ta;
    private Trip t1;
    private Person a1;
    private Person a2;
    private Person a3;
    private Person a4;
    private Group g1;
    private Group g2;

    @BeforeEach
	void setUp() throws Exception {
        ta = new TravelAgency();
        t1 = ta.add("Carros de Foc", 10, "10/06/2021", "20/06/2021", 30, false);
        a1 = new Adult("11", "Joan Grau");
        a2 = new Adult("22", "Roger Garcia");
        a3 = new Adult("33", "Lluís Borrell");
        a4 = new Adult("44", "Marta Gómez");
        g1 = new Group();
        g2 = new Group();
	}

    @Test
    public void testgetAttendeesNoGroupsAttendees() {
        Assert.assertEquals(null, ta.getAttendees(t1.getId()));
    }

    @Test
    public void testgetAttendeesNoAttendees() {
        Assert.assertTrue(t1.add(g1));
        Assert.assertEquals(null, ta.getAttendees(t1.getId()));
    }

    @Test
    public void testgetAttendeesOneAttendee() {
        Assert.assertTrue(g1.add(a1));
        Assert.assertTrue(t1.add(g1));
        HashSet<Person> people = new HashSet<Person>();
        people.add(a1);
        Assert.assertEquals(people, ta.getAttendees(t1.getId()));
    }

    @Test
    public void testgetAttendeesSomeAttendees() {
        Assert.assertTrue(g1.add(a1));
        Assert.assertTrue(g1.add(a2));
        Assert.assertTrue(t1.add(g1));
        HashSet<Person> people = new HashSet<Person>();
        people.add(a1);
        people.add(a2);
        Assert.assertEquals(people, ta.getAttendees(t1.getId()));
    }

    @Test
    public void testgetAttendeesSomeGroups() {
        Assert.assertTrue(g1.add(a1));
        Assert.assertTrue(g1.add(a2));
        Assert.assertTrue(g2.add(a3));
        Assert.assertTrue(g2.add(a4));
        Assert.assertTrue(t1.add(g1));
        Assert.assertTrue(t1.add(g2));
        HashSet<Person> people = new HashSet<Person>();
        people.add(a1);
        people.add(a2);
        people.add(a3);
        people.add(a4);
        Assert.assertEquals(people, ta.getAttendees(t1.getId()));
    }


}
